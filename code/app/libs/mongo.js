const mongoose = require('mongoose');

let _connection;

const createConnection = () => {
    _connection = mongoose.connect('mongodb://mongo/bushimen_stats');
}

const getConnection = () => {
    return _connection;
}

module.exports = {
    createConnection,
    getConnection
}