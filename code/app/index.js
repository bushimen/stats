// Database connection
const {createConnection} = require('./libs/mongo')
createConnection();

// Http
const express = require('express')
const http = require('http')
const app = express()

const bilibiliRouter = require('./entities/bilibili/router')

app.use('/api/bilibili', bilibiliRouter)

http.createServer(app).listen(3001);