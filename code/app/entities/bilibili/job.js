const request = require('request-promise'),
    {Video, Comment} = require('./model')

const fetchAllVideos = () => {
    const options = {
        uri: "https://space.bilibili.com/ajax/member/getSubmitVideos?mid=24764396&pagesize=100",
        headers: {
            'User-Agent': 'Request-Promise'
        },
        json: true
    };

    request(options)
        .then(res => {
            if (res && res.data && res.data.vlist) {
                res.data.vlist.forEach(data => {
                    Video.findOneAndUpdate(
                        {_id: data.aid},
                        {data: data},
                        (err, doc) => {
                            if (!doc) {
                                Video.create({
                                    _id: data.aid,
                                    data: data
                                });
                            }
                        }
                    );
                })
            }
        })
        .catch(err => {
            return 'err';
        })

    return 'Fetching all videos...';
}

const fetchVideoComments = (vid) => {
    const options = {
        uri: "https://api.bilibili.com/x/v2/reply?type=1&oid=" + vid,
        headers: {
            'User-Agent': 'Request-Promise'
        },
        json: true
    };

    const saveComment = data => {
        const replies = Object.assign({}, data.replies);

        delete data.member;
        delete data.replies;

        Comment.findById(data.rpid, (err, doc) => {
            if (!doc) {
                Comment.create({
                    _id: data.rpid,
                    vid: data.oid,
                    data
                })
            }
        });

        if (replies && replies.length > 0) {
            replies.forEach(reply => {
                saveComment(reply);
            })
        }
    }

    request(options)
        .then(res => {
            if (res && res.data && res.data.replies) {
                res.data.replies.forEach(comment => {
                    saveComment(comment);
                })
            }
        })
        .catch(err => {
            return 'err';
        })

    return 'Fetching all comments for video ' + vid + '...';
}

module.exports = {
    fetchAllVideos,
    fetchVideoComments
}