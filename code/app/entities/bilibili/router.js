const express = require('express'),
    router = express.Router(),
    {Video, Barrage, Comment} = require('./model')

router.get('/videos', (req, res) => {
    Video.find().exec((err, videos) => {
        res.json(videos);
    })
})

router.get('/videos/:id', (req, res) => {
    Video.findById(req.params.id).exec((err, video) => {
        res.json(video);
    })
})

module.exports = router