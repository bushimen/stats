const mongoose = require('mongoose');
    Schema = mongoose.Schema;

const Video = mongoose.model('bilibili_video', new Schema({
    _id: String,
    data: Schema.Types.Mixed
}));

const Barrage = mongoose.model('bilibili_barrage', new Schema({
    _id: String,
    vid: String,
    data: Schema.Types.Mixed
}));

const Comment = mongoose.model('bilibili_comment', new Schema({
    _id: String,
    vid: String,
    data: Schema.Types.Mixed
}));

module.exports = {
    Video,
    Barrage,
    Comment
}