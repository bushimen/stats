import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import Home from './pages/home.vue'
import BilibiliIndex from './pages/bilibili/index.vue'

const routes = [
    { path: '/', component: Home },
    { path: '/bilibili', component: BilibiliIndex }
]

export default new VueRouter({
    mode: 'history',
    routes
})