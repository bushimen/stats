import './bootstrap'

import Vue from 'vue'
import VueMaterial from 'vue-material';
import router from './router'
import App from './containers/app.vue'

Vue.use(VueMaterial);

const app = new Vue({
    router,
    template: `<App/>`,
    components: {App}
}).$mount('#app')